# Programación Lógica y Funcional -SCC1019-ISA-20213

## Actividad: Programación declarativa y funcional

### Programanción declarativa. Orientaciones y pautas para el estudio (1998,1999,2001)

```plantuml 
@startmindmap 
<style>
mindmapDiagram{
   
    BackGroundColor  	white
    LineBorder none
    LineColor red
    node{
        LineColor none
        
        BackGroundColor  white
    }

}
</style>

*[#ff6961] Programación Declarativa 

 *_ es
  *[#84b6f4] Un estilo de programación
  *[#84b6f4] Un paradigma dominante de un conjunto de dominios diversos y extensivos 

 *_ surge
  *[#84b6f4] Como una reacción a los problemas de la programación clásica 
   
 *_ su idea es
  *[#84b6f4] Liberarse de las asignaciones
  *[#84b6f4] Liberarse de las detalles del control de la gestion de memoria del ordenador
   *_ para 
    *[#fabfb7] Utilizar otros recursos expresivos para especificar los programas 

 *_ sus 
  *[#84b6f4] Ventajas 
   *_ son
    *[#fabfb7] Programas mas cortos
    *[#fabfb7] Faciles de realizar
    *[#fabfb7] Faciles de depurar
    *[#fabfb7] Reduce el riesgo de cometer error
    *[#fabfb7] Reducir la complejidad de los programas
  
  *[#84b6f4] Desventajas 
   *_ son
    *[#fabfb7] Ideas abstractas 
    *[#fabfb7] La eficiencia de los programas puede no ser igual a la idea del programador
    *[#fabfb7] Se tiene que saber a que nivel de programación se tiene que trabajar

 *[#84b6f4] Variantes
  *[#fabfb7] Programación funcional
   *_ es
    *[#b0f2c2] Recurrir al lenguaje lenguaje que utilizan los matemáticos 
    *[#b0f2c2] Recurrir al lenguaje que se utiliza al escribir funciones 
   *_ hace
    *[#b0f2c2] Tomar el modelo en el que los matemáticos definen funciones.
    *[#b0f2c2] La aplicación de unas funciones a unos datos
     *_ se hace mediante 
      *[#fdf9c4] Reglas de reescritura 
      *[#fdf9c4] Simplificación de expresiones 
   *_ características
    *[#b0f2c2] Evaluacion peresoza
     *_ consiste
      *[#fdf9c4] Los calculos no se realizan asta que otro posterior lo necesita
      *[#fdf9c4] Solamente evaluar las operaciones, calculca o computa solo aquello que es estrictamente necesario para el programa
      *[#fdf9c4] Definir tipos de datos infinitos
    *[#b0f2c2] Funciones de orden superior
     *_ es 
      *[#fdf9c4] La aplicacion de funciones sobres datos
       *_ se aplican a
        *[#fdcae1] Programas a datos
        *[#fdcae1] Pogramas a otros programas que modifican datos 
       
  *[#fabfb7] Programación lógica
   *_ recurre a
    *[#b0f2c2] Acude a la logica de predicados de primer orden
     *_ es
      *[#fdf9c4] la relacion entre objetos
     *_ permite
      *[#fdf9c4] Ser mas declarativos
       *[#fdcae1] Puesto que no estamos estableciendo la direccion de rediccionamiento  
   *_ se basa en
    *[#b0f2c2] El modelo de la demostración de la lógica
    *[#b0f2c2] Demostración automatica
 *_ consiste en
  *[#84b6f4] Dejar las tareas rutinarios del programador al conmpilador
  *[#84b6f4] Decirle a un programa lo que tiene que hacer en lugar de decirle cómo debería hacerlo
  *[#84b6f4] Implicar o proporcionar un lenguaje específico de dominio para expresar lo qué el usuario quiere 
   *_ utiliza
    * Construcciones de bajo nivel
     *_ como
      * Bucles
      * Condiciones
      * Asignaciones
  
  
 
   
  @endmindmap 
  ```

#### Fuentes de información:

[Programación Declarativa. Orientaciones y pautas para el estudio (1998)](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)

[Programación Declarativa. Orientaciones y pautas para el estudio (1999)](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

[Programación Declarativa. Orientaciones y pautas para el estudio (2001)](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

### Lenguaje de Programación Funcional (2015)

```plantuml 
@startmindmap 
<style>
mindmapDiagram{
   
    BackGroundColor  	white
    LineBorder black
    LineColor red
    node{
        LineColor Black
        
        BackGroundColor  #E15566
    }

}
</style>

*[#FFBADE] Lenguaje de Programación Funcional
 *_ es
  *[#BEDBAB] Un paradigma de programación
   *[#FFE2C8] donde el modelo de computación de los diferentes lenguajes
    *_ que
     *[#DC92EF] Dotan de semántica a los programas 
   *_ se basa en
    *[#FFE2C8] El modelo de computación de von Neumann
     *[#DC92EF] significa que
      * Todo lo que se puede hacer en uno, se puede hacer en otros

 *[#BEDBAB] Variante
  *_ es
   *[#FFE2C8] Programación orientada a objetos
    *_ son
     *[#DC92EF] Pequeños trozos de codigos o objetos que interactuan entre si
      *_ se componen de
       * Instrucciones que ese ejecutan secuencialmente

 *_ su base es
  *[#BEDBAB] Programación imperativa
   *_ define a un programa como
    *[#FFE2C8] Una colección de datos
    *[#FFE2C8] Colección de instrucciones que operan sobre dichos datos
    *[#FFE2C8] Las instrucciones se ejecutan en un orden adecuado
    *[#FFE2C8] El orden se modifica segun el valor de las variables o estado del computo
    *[#FFE2C8] La ejecución de las instrucciones realiza un computo

  *[#BEDBAB] Programación funcional
   *_ sus características son
    *[#FFE2C8] Son funciones
    *[#FFE2C8] Se gira en torno a funciones 
    *[#FFE2C8] Recibe funciones como entradas
    *[#FFE2C8] Tiene funciones de orden superior
    
   *_ sus consecuencias son
    *[#FFE2C8] Uso del concepto de asignación
    *[#FFE2C8] Conceptos que desaparecen 
     *_ como
      *[#DC92EF] Bucles 
       *_ cambia a
        * Recursión
      *[#DC92EF] Variables
   *_ cuando
    * El programa es una funcion
     *_ utiliza 
      * Las variables como parametros de entrada
     *_ usa
      * La transparencia referencial
       *_ son
        * Los resultados independientes
     *_ añade
      * La evaluación perezosa
       *_ es  
        * Una evaluacion de afuera hacia adentro 
       *_ utiliza
        * La memorización
         *_ sirve para
          * Almacenar el valor de las operaciones
       

 *_ Utiliza la
  *[#BEDBAB] Certificación
   *_ es
    *[#FFE2C8] Una función
     *_ con
      *[#DC92EF] Varios parámetros de entrada
     *_ devuelve 
      *[#DC92EF] Diferentes salidas  

 *_ se contrasta con
  *[#BEDBAB] El lenguaje imperativo
   *_ es:
    *[#FFE2C8] Un conjunto de instrucciones que le indican al computador como realizar una tarea.
   *_ donde
    *[#FFE2C8] Describren la programación en términos del estado del programa y sentencias que cambian dicho estado. 

 *_ utiliza 
  *[#BEDBAB] Memorización
   *_ donde 
    *[#FFE2C8] Almacena el valor de una expresión cuya evaluación ya se realizo


  @endmindmap 
  ```
#### Fuente de información:

[Lenguaje de Programación Funcional (2015)](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)

Autor:
[RomanHernandezEstebanDaniel @ GitLab](https://gitlab.com/RomanHernandezEstebanDaniel)
